// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl:  'http://localhost:4606/api',
  // apiUrl:  'http://93.188.167.68:4606/api',
  name: 'dev',
  firebase : {
    apiKey: "AIzaSyBro5H5yXNAwB12IsYJI9A4UX8nG3rsD-8",
    authDomain: "swap-signal-staff.firebaseapp.com",
    projectId: "swap-signal-staff",
    storageBucket: "swap-signal-staff.appspot.com",
    messagingSenderId: "290682872988",
    appId: "1:290682872988:web:7d4c2a43d6b5237a6a7959",
    measurementId: "G-HLFREZL5VF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
