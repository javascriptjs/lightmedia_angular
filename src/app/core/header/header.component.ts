import { AuthenticationService } from 'src/app/shared/services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  currentUrl: any;
  userId: any;
  userData: any;

  constructor(
    private router: Router,
    public apiService: ApiService,
    public authenticationService : AuthenticationService,
    ) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        this.currentUrl = event.url.replace(/\//g, '');
      });
    this.userId = localStorage.getItem('userId');

  }

  ngOnInit(): void {
    this.getUser();
  }

  getShortName(fullName: any) {
    return fullName.split(' ').map((n: any) => n[0]).join('');
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/sign-in']);
  }

  getUser() {
    this.apiService.currentUser(this.userId).subscribe((arg: any) => {
      this.userData = arg.data;
    }, (error) => {
      console.error('error', error);
    });
  }


  getProfile() {
    this.router.navigate([`/profile/${this.userId}`]);
  }
}
