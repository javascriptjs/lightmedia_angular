import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication/authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  hide = true;
  authentiation: string | null;

  constructor(
    private router: Router,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private authenticationService: AuthenticationService
  ) {
    this.authentiation = localStorage.getItem('Swap-Signal-Staff');
    if (this.authentiation) {
      this.router.navigate(['/dashboard']);
    }
  }

  ngOnInit() {}

  login() {
    if (this.form.valid) {
      this.authenticationService
        .login(this.form.value.email, this.form.value.password)
        .pipe(first())
        .subscribe(
          (data) => {
            this.router.navigate(['/dashboard']);
          },
          (error) => {
            this.spinner.hide();
            this.toastr.error(error);
          }
        );
    }
  }

}
