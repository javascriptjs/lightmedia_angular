import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication.service';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    authentiation: any;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authentiation = localStorage.getItem('Swap-Signal-Staff');
     }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let currentUser = this.authenticationService.currentUserValue;
        if(this.authentiation){
           currentUser = {token: this.authentiation}
        }
        if (currentUser && currentUser.token) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/auth/sign-in']);
        return false;
    }
}
