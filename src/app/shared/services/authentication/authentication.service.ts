import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from '../local-storage.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  _user: any;

  constructor(
    private http: HttpClient,
    public toastr: ToastrService,
    private store: LocalStorageService,
    private spinner: NgxSpinnerService
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(
      localStorage.getItem('token') || '{}'
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  private setUser(user: any) {
    if (user) {
      this.store.setItem('apiToken', user.token);
      this.store.setItem('userId', user._id);
      this.store.setItem('Swap-Signal-Staff', 'true');
    } else {
      this.store.clear();
    }
    this._user = user;
    if (user && user.token) {
      this.currentUserSubject.next({ token: user.token });
    } else {
      this.currentUserSubject.next(user);
    }
  }

  login(email: string, password: string) {
    this.spinner.show();
    return this.http
      .post<any>(`${environment.apiUrl}/users/login`, { email, password })
      .pipe(
        map((user) => {
          if (user.data.role == 'admin') {
            this.toastr.success('Successfully LoggedIn!!!');
            this.spinner.hide();
            this.setUser(user.data);
            return user;
          } else {
            this.toastr.error('Role is not Admin!!!');
            this.spinner.hide();
          }
        })
      );
  }

  logout() {
    this.setUser(null);
    this.toastr.success('Successfully LogOut!!');
  }
}
