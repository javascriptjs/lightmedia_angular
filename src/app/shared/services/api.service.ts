import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  root = environment.apiUrl;
  userResponse: any;
  usersData: any;
  categoryResponse: any;
  tagResponse: any;
  token: any = '';

  constructor(
    private http: HttpClient,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.token = localStorage.getItem('apiToken');
  }
  // --------------------access token------------------------
  getHeaders() {
    let header;
    if (this.token != '') {
      header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'x-access-token': this.token,
        }),
      };
    } else {
      console.log('token not found');
    }
    return header;
  }

  createUser(body: any): Observable<any> {
    const subject = new Subject<any>();
    this.http.post(`${this.root}/users/create`, body).subscribe(
      (responseData) => {
        this.userResponse = responseData;
        subject.next(this.userResponse);
      },
      (error) => {
        subject.next(error);
      }
    );
    return subject.asObservable();
  }

  createCoin(body: any): Observable<any> {
    const subject = new Subject<any>();
    this.http.post(`${this.root}/coins/create`, body).subscribe(
      (responseData) => {
        this.userResponse = responseData;
        subject.next(this.userResponse);
      },
      (error) => {
        subject.next(error);
      }
    );
    return subject.asObservable();
  }

  createPlan(body: any): Observable<any> {
    const subject = new Subject<any>();
    this.http.post(`${this.root}/plans/create`, body).subscribe(
      (responseData) => {
        this.userResponse = responseData;
        subject.next(this.userResponse);
      },
      (error) => {
        subject.next(error);
      }
    );
    return subject.asObservable();
  }

  forgotPassword(model: any): Observable<any> {
    this.spinner.show();
    const subject = new Subject<any>();
    this.http.post(`${this.root}/users/forgotPassword`, model).subscribe(
      (responseData: any) => {
        subject.next(responseData);
        this.spinner.hide();
        this.toastr.success('Check your email');
      },
      (err) => {
        subject.next(err);
        this.spinner.hide();
        this.toastr.error(err.error.error);
      }
    );
    return subject.asObservable();
  }

  getUsers(): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.spinner.show();
    this.http
      .get(
        `${this.root}/users/getUsers?pageNo=1&pageSize=1000`,
        this.getHeaders()
      )
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
          this.spinner.hide();
          // this.toastr.success('Get Users List Successfully');
        },
        (error) => {
          subject.next(error);
          this.spinner.hide();
          // this.toastr.error(error);
        }
      );
    return subject.asObservable();
  }

  getCoins(): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.spinner.show();
    this.http
      .get(
        `${this.root}/coins/getCoins?pageNo=1&pageSize=1000`,
        this.getHeaders()
      )
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
          this.spinner.hide();
          // this.toastr.success('Get Users List Successfully');
        },
        (error) => {
          subject.next(error);
          this.spinner.hide();
          // this.toastr.error(error);
        }
      );
    return subject.asObservable();
  }


  getPlans(): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.spinner.show();
    this.http.get(`${this.root}/plans/getPlans`, this.getHeaders()).subscribe(
      (responseData) => {
        this.userResponse = responseData;
        subject.next(this.userResponse);
        this.spinner.hide();
        // this.toastr.success('Get Users List Successfully');
      },
      (error) => {
        subject.next(error);
        this.spinner.hide();
        // this.toastr.error(error);
      }
    );
    return subject.asObservable();
  }

  currentUser(id: any): Observable<any[]> {
    this.spinner.show();
    const subject = new Subject<any[]>();
    this.http
      .get(`${this.root}/users/currentUser/${id}`, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          this.spinner.hide();
          // this.toastr.success('Get current User Successfully');
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
          this.spinner.hide();
          // this.toastr.error(error);
        }
      );
    return subject.asObservable();
  }

  getPlan(id: any): Observable<any[]> {
    this.spinner.show();
    const subject = new Subject<any[]>();
    this.http
      .put(`${this.root}/plans/getById/${id}`, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          this.spinner.hide();
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
          this.spinner.hide();
        }
      );
    return subject.asObservable();
  }

  updateUser(id: any, model: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .put(`${this.root}/users/update/${id}`, model, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          this.toastr.success('User Updated Successfully!!');
          subject.next(this.userResponse);
        },
        (error) => {
          this.toastr.error(error);
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  updateCoin(id: any, model: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .put(`${this.root}/coins/update/${id}`, model, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          this.toastr.success('User Updated Successfully!!');
          subject.next(this.userResponse);
        },
        (error) => {
          this.toastr.error(error);
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  resetPassword(id: any, model: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .put(`${this.root}/users/changePassword/${id}`, model, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  deleteUser(id: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .delete(`${this.root}/users/delete/${id}`, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  deleteCoin(id: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .delete(`${this.root}/coins/delete/${id}`, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  planDelete(id: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .delete(`${this.root}/plans/delete/${id}`, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  updatePlan(id: any, model: any): Observable<any[]> {
    const subject = new Subject<any[]>();
    this.http
      .put(`${this.root}/plans/update/${id}`, model, this.getHeaders())
      .subscribe(
        (responseData) => {
          this.userResponse = responseData;
          subject.next(this.userResponse);
        },
        (error) => {
          subject.next(error);
        }
      );
    return subject.asObservable();
  }

  uploadUserImage(id: any, model: any): Observable<any> {
    const subject = new Subject<any>();
    this.http
      .put(`${this.root}/users/uploadProfilePic/${id}`, model, {
        headers: new HttpHeaders({
          enctype: 'multipart/form-data',
          Accept: 'application/json',
          'x-access-token': this.token,
        }),
      })
      .subscribe(
        (response) => {
          subject.next(response);
          console.log('response', response);
        },
        (error) => {
          console.log('error', error);
          subject.next(error.error);
        }
      );
    return subject.asObservable();
  }
}
