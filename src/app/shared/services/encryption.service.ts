import { Injectable } from '@angular/core';
var CryptoTS = require('crypto-js');

@Injectable({
  providedIn: 'root',
})
export class EncryptionService {

  tokenFromUI: string = 'DoverDigitalHydr';
  encrypted: any = '';
  decrypted: any;
  responce: any;

  constructor() {}

  encryptionAES(msg: any) {
    let _key = CryptoTS.enc.Utf8.parse(this.tokenFromUI);
    _key.words.push(_key.words[0], _key.words[1]);
    let encrypted = CryptoTS.TripleDES.encrypt(
      CryptoTS.enc.Utf8.parse(msg),
      _key,
      {
        mode: CryptoTS.mode.ECB,
        padding: CryptoTS.pad.Pkcs7,
      }
    );
    this.encrypted = encrypted.toString();
    console.log('encrypted', this.encrypted);
    return this.encrypted;
  }

  decryptionAES(msg: any) {
    let _key = CryptoTS.enc.Utf8.parse(this.tokenFromUI);
    _key.words.push(_key.words[0], _key.words[1]);
    this.decrypted = CryptoTS.TripleDES.decrypt(msg, _key, {
      mode: CryptoTS.mode.ECB,
      padding: CryptoTS.pad.Pkcs7,
    }).toString(CryptoTS.enc.Utf8);
    console.log('decrypted', this.decrypted);
    return this.decrypted;
  }
}
