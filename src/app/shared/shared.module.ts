import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './services/authentication/authentication.service';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmationDialogService } from './services/confirmation-dialog.service';

@NgModule({
  declarations: [ConfirmationDialogComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule, MatDialogModule],
  providers: [AuthenticationService, ConfirmationDialogService],
})
export class SharedModule {}
