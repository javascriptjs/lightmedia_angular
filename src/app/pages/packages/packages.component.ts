import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css'],
})
export class PackagesComponent implements OnInit {
  basicPlan: boolean = false;
  advancedPlan: boolean = false;
  premiumPlan: boolean = false;

  selectedPlan: any;

  basic: any = {
    name: 'Basic Plan',
    amount: '8',
  };

  advanced: any = {
    name: 'Advanced Plan',
    amount: '18',
  };

  premium: any = {
    name: 'Premium Plan',
    amount: '28',
  };

  isAdd: boolean = false;
  planList: any;

  form = new FormGroup({
    planName: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required]),
  });

  constructor(
    public apiService: ApiService,
    private confirmationDialogService: ConfirmationDialogService
  ) {}

  ngOnInit(): void {
    this.getPlans();
  }

  createPlan() {
    if (this.form.valid) {
      this.apiService.createPlan(this.form.value).subscribe((res: any) => {
        if (res.data) {
          this.getPlans();
          this.isAdd = false;
        }
      });
    }
  }

  getPlans() {
    this.apiService.getPlans().subscribe((res: any) => {
      this.planList = res.data;
    });
  }

  selectPlan(selected: any) {
    this.selectedPlan = selected;
    // if (selected.planName === 'Basic Plan') {
    //   this.basicPlan = true;
    //   this.advancedPlan = false;
    //   this.premiumPlan = false;
    // } else if (selected.planName === 'Advanced Plan') {
    //   this.advancedPlan = true;
    //   this.basicPlan = false;
    //   this.premiumPlan = false;
    // } else if (selected.planName === 'Premium Plan') {
    //   this.premiumPlan = true;
    //   this.basicPlan = false;
    //   this.advancedPlan = false;
    // }
  }

  deletePlan(id: any) {
    this.confirmationDialogService
      .confirm({
        title: 'Please confirm..',
        message: 'Do you really want to Delete ... ?',
      })
      .subscribe((result: any) => {
        if (result) {
          this.apiService.planDelete(id).subscribe((res: any) => {
            if (res.data) {
              this.getPlans();
              this.selectedPlan = false;
            }
          });
        }
      });
  }

  updatePlan() {
    this.apiService
      .updatePlan(this.selectedPlan._id, this.selectedPlan)
      .subscribe((res: any) => {
        if (res.data) {
          this.getPlans();
          this.selectedPlan = null;
        }
      });
  }

  backEdit() {
    this.basicPlan = false;
    this.advancedPlan = false;
    this.premiumPlan = false;
    this.selectedPlan = null;
  }

  addPlan() {
    this.isAdd = true;
  }

  back() {
    this.isAdd = false;
  }
}
