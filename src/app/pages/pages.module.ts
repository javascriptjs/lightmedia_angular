import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { ContentComponent } from './content/content.component';
import { CoreModule } from '../core/core.module';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatMenuModule } from '@angular/material/menu';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { UsersListComponent } from './users-list/users-list.component';
import { ChartsModule } from 'ng2-charts';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ReportsComponent } from './reports/reports.component';
import { PackagesComponent } from './packages/packages.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../shared/shared.module';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatSelectModule} from '@angular/material/select';
import { CoinsComponent } from './coins/coins.component';
import { AddEditCoinsComponent } from './components/add-edit-coins/add-edit-coins.component';

@NgModule({
  declarations: [
    ContentComponent,
    DashboardComponent,
    UserProfileComponent,
    UsersListComponent,
    ReportsComponent,
    PackagesComponent,
    CoinsComponent,
    AddEditCoinsComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    CoreModule,
    SharedModule,
    MatTooltipModule,
    MatCardModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTableModule,
    ChartsModule,
    MatProgressBarModule,
    MatButtonModule,
    MatGridListModule,
  ],
  exports: [UserProfileComponent],
})
export class PagesModule {}
