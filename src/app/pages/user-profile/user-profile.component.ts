import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
// import { EncryptionService } from 'src/app/shared/services/encryption.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  form = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    status: new FormControl(''),
  });

  isEdit: boolean = false;
  userId: any;
  userData: any;
  imgResponse: any;
  formData = new FormData();
  fileData!: File;

  constructor(
    public apiService: ApiService,
    private router: Router,
    // private encrypt: EncryptionService,
    private activatedRoute: ActivatedRoute
  ) {
    this.userId = this.activatedRoute.snapshot.params['id'];
    // this.userId = this.encrypt.decryptionAES(this.userId);
  }

  ngOnInit(): void {
    this.getUser();
    // const encrypted = this.encrypt.encryptionAES('hello world');
    // console.log(encrypted);
    // console.log('this.userId',this.userId);
  }

  getShortName(fullName: any) {
    return fullName
      .split(' ')
      .map((n: any) => n[0])
      .join('');
  }

  update() {
    if (this.form.valid) {
      this.apiService.updateUser(this.userId, this.form.value).subscribe(
        (arg: any) => {
          if (arg.data) {
            this.getUser();
          }
        },
        (error) => {
          console.error('error', error);
        }
      );
    }
  }

  getUser() {
    this.apiService.currentUser(this.userId).subscribe(
      (arg: any) => {
        this.userData = arg.data;
      },
      (error) => {
        console.error('error', error);
      }
    );
  }

  edit() {
    this.isEdit = true;
  }

  back() {
    this.router.navigate([`/users`]);
  }

  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.fileData = event.target.files[0];
      this.formData.append('image', this.fileData);
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.userData.avatar = event.target.result;
        this.uploadProfile();
      };
    }
  }

  uploadProfile() {
    this.apiService
      .uploadUserImage(this.userId, this.formData)
      .subscribe((res: any) => {
        this.imgResponse = res;
      });
  }
}
