import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-add-edit-coins',
  templateUrl: './add-edit-coins.component.html',
  styleUrls: ['./add-edit-coins.component.css'],
})
export class AddEditCoinsComponent implements OnInit {
  form = new FormGroup({
    coinName: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required]),
    coinCode: new FormControl('', [Validators.required]),
  });

  constructor(
    public apiService: ApiService,
    public dialogRef: MatDialogRef<AddEditCoinsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  back(): void {
    this.dialogRef.close();
  }

  add() {
    this.apiService.createCoin(this.form.value).subscribe((res: any) => {
      // this.dataSource = res.data;
      this.dialogRef.close(true);
    });
  }

  update() {
    this.apiService.updateCoin(this.data.id, this.form.value)
      .subscribe((res: any) => {
        this.dialogRef.close(true);
      });
  }
}
