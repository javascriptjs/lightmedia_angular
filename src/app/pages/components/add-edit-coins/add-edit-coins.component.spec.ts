import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCoinsComponent } from './add-edit-coins.component';

describe('AddEditCoinsComponent', () => {
  let component: AddEditCoinsComponent;
  let fixture: ComponentFixture<AddEditCoinsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditCoinsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCoinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
