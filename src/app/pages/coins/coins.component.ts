import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { AddEditCoinsComponent } from '../components/add-edit-coins/add-edit-coins.component';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-coins',
  templateUrl: './coins.component.html',
  styleUrls: ['./coins.component.css'],
})
export class CoinsComponent implements OnInit {
  displayedColumns: string[] = ['no','logo', 'coinName', 'price', 'action'];
  dataSource: any;

  constructor(
    public apiService: ApiService,
    private confirmationDialogService: ConfirmationDialogService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getCoins();
  }

  getCoins() {
    this.apiService.getCoins().subscribe((res: any) => {
      this.dataSource = res.data;
    });
  }

  deleteCoin(item: any) {
    this.confirmationDialogService
      .confirm({
        title: 'Please confirm..',
        message: 'Do you really want to Delete ... ?',
      })
      .subscribe((result: any) => {
        if (result) {
          this.apiService.deleteCoin(item._id).subscribe((res: any) => {
            // this.dataSource = res.data;
            this.getCoins();
          });
        }
      });
  }


  getShortName(fullName: any) {
    return fullName
      .split(" ")
      .map((n: any) => n[0])
      .join("");
  }

  add() {
    const dialogRef = this.dialog.open(AddEditCoinsComponent, {
      width: '550px',
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.getCoins();
      }
    });
  }

  edit(item: any): void {
    const dialogRef = this.dialog.open(AddEditCoinsComponent, {
      width: '550px',
      data: {
        id: item._id,
        coinName: item.coinName,
        amount: item.amount,
        coinCode: item.coinCode,
      },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.getCoins();
      }
    });
  }
}
