import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';

export interface PeriodicElement {
  name: string;
  date: string;
  plan: string;
  amount: string;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    date: '01/06/21',
    name: 'Hydrogen',
    plan: 'Basic',
    amount: '$ 8',
    description: `Hydrogen is a chemical element with amount H and atomic number 1. With a standard
        atomic plan of 1.008, hydrogen is the lightest element on the periodic table.`
  }, {
    date: '02/06/21',
    name: 'Helium',
    plan: 'Basic',
    amount: '$ 8',
    description: `Helium is a chemical element with amount He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`
  }, {
    date: '03/06/21',
    name: 'Lithium',
    plan:'Basic',
    amount: '$ 8',
    description: `Lithium is a chemical element with amount Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`
  }, {
    date: '04/06/21',
    name: 'Beryllium',
    plan:'Standard',
    amount: '$ 18',
    description: `Beryllium is a chemical element with amount Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`
  }, {
    date: '05/06/21',
    name: 'Boron',
    plan: 'Premium',
    amount: '$ 28',
    description: `Boron is a chemical element with amount B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`
  }, {
    date: '06/06/21',
    name: 'Carbon',
    plan: 'Basic',
    amount: '$ 8',
    description: `Carbon is a chemical element with amount C and atomic number 6. It is nonmetallic
        and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
        to group 14 of the periodic table.`
  }, {
    date: '07/06/21',
    name: 'Nitrogen',
    plan: 'Standard',
    amount: '$ 18',
    description: `Nitrogen is a chemical element with amount N and atomic number 7. It was first
        discovered and isolated by Scottish physician Daniel Rutherford in 1772.`
  }, {
    date: '08/06/21',
    name: 'Oxygen',
    plan: 'Basic',
    amount: '$ 8',
    description: `Oxygen is a chemical element with amount O and atomic number 8. It is a member of
         the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
         agent that readily forms oxides with most elements as well as with other compounds.`
  }, {
    date: '09/06/21',
    name: 'Fluorine',
    plan: 'Premium',
    amount: '$ 28',
    description: `Fluorine is a chemical element with amount F and atomic number 9. It is the
        lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
        conditions.`
  }, {
    date: '10/06/21',
    name: 'Neon',
    plan: 'Premium',
    amount: '$ 28',
    description: `Neon is a chemical element with amount Ne and atomic number 10. It is a noble gas.
        Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
        two-thirds the density of air.`
  },
];

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class ReportsComponent implements OnInit {

  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['name', 'plan', 'amount', 'date'];
  expandedElement!: PeriodicElement | null;
  // rows: any = [
  //   { total: 65, label: '2006' },
  //   { total: 59, label: '2007' },
  //   { total: 80, label: '2008' },
  //   { total: 81, label: '2009' },
  //   { total: 56, label: '2010' },
  //   { total: 55, label: '2011' },
  //   { total: 40, label: '2012' },
  // ];

  // public barChartOptions: ChartOptions = {
  //   responsive: true,
  // };

  // public barChartLabels: Label[] = [
  //   '2006',
  //   '2007',
  //   '2008',
  //   '2009',
  //   '2010',
  //   '2011',
  //   '2012',
  // ];
  // public barChartType: ChartType = 'bar';
  // public barChartLegend = true;
  // public barChartPlugins = [];

  // public barChartData: ChartDataSets[] = [
  //   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  //   { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  // ];

  // public LineChartData: ChartDataSets[] = [
  //   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  // ];

  // pieChartOptions!: ChartOptions;
  // pieChartLabels!: Label[];
  // pieChartData!: SingleDataSet;
  // pieChartType!: ChartType;
  // pieChartLegend!: boolean;
  // pieChartPlugins = [];
  // countryTrafficStats!: any[];

  // chartColors: Array<any> = [{
  //   backgroundColor: 'rgba(34, 42, 69, 0.96)',
  //   borderColor: 'rgba(34, 42, 69, 0.96)',
  //   pointBackgroundColor: 'rgba(34, 42, 69, 0.96)',
  //   pointBorderColor: '#fff',
  //   pointHoverBackgroundColor: '#fff',
  //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  // }];

  constructor() {}

  ngOnInit(): void {
    // this.pieChartOptions = this.createOptions();
    // this.pieChartLabels = ['January', 'February', 'March'];
    // this.pieChartData = [50445, 33655, 15900];
    // this.pieChartType = 'pie';
    // this.pieChartLegend = true;
    // // this.pieChartPlugins = [pluginLabels];

    // this.countryTrafficStats = [
    //   {
    //     country: "US",
    //     visitor: 14040,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 30,
    //     flag: "flag-icon-us"
    //   },
    //   {
    //     country: "India",
    //     visitor: 12500,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 45,
    //     flag: "flag-icon-in"
    //   },
    //   {
    //     country: "UK",
    //     visitor: 11000,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 50,
    //     flag: "flag-icon-gb"
    //   },
    //   {
    //     country: "Brazil",
    //     visitor: 4000,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 30,
    //     flag: "flag-icon-br"
    //   },
    //   {
    //     country: "Spain",
    //     visitor: 4000,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 45,
    //     flag: "flag-icon-es"
    //   },
    //   {
    //     country: "Mexico",
    //     visitor: 4000,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 70,
    //     flag: "flag-icon-mx"
    //   },
    //   {
    //     country: "Russia",
    //     visitor: 4000,
    //     pageView: 10000,
    //     download: 1000,
    //     bounceRate: 40,
    //     flag: "flag-icon-ru"
    //   }
    // ];
  }


  // private createOptions(): ChartOptions {
  //   return {
  //     responsive: true,
  //     maintainAspectRatio: true,
  //     plugins: {
  //       labels: {
  //         render: 'percentage',
  //         fontColor: ['green', 'white', 'red'],
  //         precision: 2,
  //       },
  //     },
  //   };
  // }
}
