import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './../../shared/services/api.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
// import { EncryptionService } from 'src/app/shared/services/encryption.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
})
export class UsersListComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'email',
    'package',
    'status',
    'action',
  ];

  userList: any;

  constructor(
    private router: Router,
    // private encrypt: EncryptionService,
    private confirmationDialogService: ConfirmationDialogService,
    public apiService: ApiService
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  edit(element: any) {
    // element._id = this.encrypt.encryptionAES(element._id);
    // const decrypted = this.encrypt.decryptionAES(this.userId);
    // let id =  encrypted.replace(/\//g, '');
    // console.log(decrypted);
    this.router.navigate([`/profile/${element._id}`]);
  }

  deleteUser(id: any) {
    this.confirmationDialogService
      .confirm({
        title: 'Please confirm..',
        message: 'Do you really want to Delete ... ?',
      })
      .subscribe((result: any) => {
        if (result) {
          this.apiService.deleteUser(id).subscribe((res: any) => {
            this.getUsers();
          });
        }
      });
  }

  getUsers() {
    this.apiService.getUsers().subscribe((res: any) => {
      this.userList = res.data;
    });
  }
}
