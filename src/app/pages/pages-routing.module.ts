import { PackagesComponent } from './packages/packages.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersListComponent } from './users-list/users-list.component';
import { ReportsComponent } from './reports/reports.component';
import { AuthGuard } from '../shared/services/authentication/_helpers';
import { CoinsComponent } from './coins/coins.component';

const routes: Routes = [
  {
    path: '',
    component: ContentComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          breadcrumb: 'dashboard',
        },
      },
      {
        path: 'users',
        component: UsersListComponent,
        data: {
          breadcrumb: 'users',
        },
      },
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          breadcrumb: 'reports',
        },
      },
      {
        path: 'profile/:id',
        component: UserProfileComponent,
        data: {
          breadcrumb: 'profile',
        },
      },
      {
        path: 'packages',
        component: PackagesComponent,
        data: {
          breadcrumb: 'packages',
        },
      },
      {
        path: 'coins',
        component: CoinsComponent,
        data: {
          breadcrumb: 'coins',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
